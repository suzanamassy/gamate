//
//  Settings.swift
//  Gamers
//
//  Created by Suzan Amassy on 4/19/18.
//  Copyright © 2018 Wasim Alatrash. All rights reserved.
//

import Foundation


// MARK: - To Store All Important Data in UserDefault
final class Settings: NSObject, NSCoding {
    
    class var shared: Settings {
        struct Static {
            static var instance: Settings? = nil
        }
        
        if Static.instance == nil {
            if let data = UserDefaults.standard.object(forKey: "com.wasim.MicGamer") as? Data {
                Static.instance = NSKeyedUnarchiver.unarchiveObject(with: data) as? Settings
            } else {
                Static.instance = Settings()
            }
            Static.instance?.finishedInit_ = true

        }
        
        return Static.instance!
    }

    
    fileprivate var finishedInit_ = false
    
    var email:String? {
        didSet {
            save()
        }
    }
    
    var password:String? {
        didSet {
            save()
        }
    }
    
    var userId: String? {
        didSet {
            save()
        }
    }
    
    var username: String? {
        didSet {
            save()
        }
    }
    
    var userImage: String? {
        didSet {
            save()
        }
    }
    
    
    fileprivate override init() {}
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(email, forKey: "email")
        aCoder.encode(password, forKey: "password")
        aCoder.encode(userId, forKey: "userId")
        aCoder.encode(username, forKey: "username")
        aCoder.encode(userImage, forKey: "userImage")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        self.init()
        email               = aDecoder.decodeObject(forKey: "email")        as? String
        password            = aDecoder.decodeObject(forKey: "password")     as? String
        userId                = aDecoder.decodeObject(forKey: "userId")         as? String
        username                = aDecoder.decodeObject(forKey: "username")         as? String
        userImage                = aDecoder.decodeObject(forKey: "userImage")         as? String
    }
    
    
    fileprivate func save() {
        guard finishedInit_ else {
            return
        }

        let data = NSKeyedArchiver.archivedData(withRootObject: self)
        UserDefaults.standard.set(data, forKey: "com.wasim.MicGamer")
    }
    
    
    internal func resetToDefaults() {
        finishedInit_ = true
        save()
    }
}

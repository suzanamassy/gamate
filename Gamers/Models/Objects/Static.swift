//
//  Privacy.swift
//  Gamers
//
//  Created by Wasim Alatrash on 4/19/18.
//  Copyright © 2018 Wasim Alatrash. All rights reserved.
//

import Foundation

struct Static : Model {
    static var collection: String { return "static" }
    
    var id: String?
    var text: String
}

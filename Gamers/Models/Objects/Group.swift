//
//  Group.swift
//  Gamers
//
//  Created by Wasim Alatrash on 4/19/18.
//  Copyright © 2018 Wasim Alatrash. All rights reserved.
//

import Foundation

struct IdenitfiableGroup : Model {
    var id: String?
    static var collection: String { return "groups" }
}

struct Group : Model {
    var id: String?
    static var collection: String { return "groups" }
    
    var creatorId: String
    var creatorName: String
    var creatorImage: String
    var players: [Player]?
    var game: String
    var country: String?
    var creationDate: Date
    var device: String
    var title: String
    var hasMic: Bool?
    var language: [String]?
    var playersNumber: Int
    var age: Int?
    var gender: String?
    var gameImage: String
}


struct Player : Codable {
    var id: String
    var image: String
    var name: String
    var level: String
}


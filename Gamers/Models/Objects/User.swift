//
//  User.swift
//  Gamers
//
//  Created by Wasim Alatrash on 4/19/18.
//  Copyright © 2018 Wasim Alatrash. All rights reserved.
//

import Foundation

struct IdenitfiableUser : Model {
    static var collection: String { return "users" }
    
    var id: String?
}

struct User : Model{
    static var collection: String { return "users" }
    
    var id: String?
    var username: String
    var birthday: Date
    var country: String
    var creationDate: Date
    var device: String
    var image: String
    var email: String
    var fcm: String?
    var hasMic: Bool
    var language: [String]
    var numberOfGroups: Int
    var numberOfJoinedPlayers: Int
}

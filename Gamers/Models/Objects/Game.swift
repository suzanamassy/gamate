//
//  User.swift
//  Gamers
//
//  Created by Wasim Alatrash on 4/19/18.
//  Copyright © 2018 Wasim Alatrash. All rights reserved.
//

import Foundation

struct IdenitfiableGame : Model {
    var id: String?
    static var collection: String { return "games" }
}

struct Game : Model {
    
    static var collection: String { return "games" }
    var id: String?
    var featured: Bool
    var groups: Int
    var image: String
    var name: String
    var top: Bool
    
}

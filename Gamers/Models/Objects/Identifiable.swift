//
//  Identifiable.swift
//  Gamers
//
//  Created by Wasim Alatrash on 4/19/18.
//  Copyright © 2018 Wasim Alatrash. All rights reserved.
//

import Foundation

protocol Identifiable {
    static var collection: String { get }
    var id: String { get }
}

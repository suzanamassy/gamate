//
//  Enum.swift
//  Gamers
//
//  Created by Suzan Amassy on 4/19/18.
//  Copyright © 2018 Wasim Alatrash. All rights reserved.
//

import Foundation


enum GroupFeature : Int {
    case mic = 1
    case country = 2
    case language = 3
    case gender = 4
    case age = 5
}



enum Gender {
    case other
    case female
    case male
}

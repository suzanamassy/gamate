//
//  Constant.swift
//  MicGamer
//
//  Created by Suzan Amassy on 4/19/18.
//  Copyright © 2018 wasimAlatrash. All rights reserved.
//

import Foundation
import ChameleonFramework


struct Color {
    static let main = UIColor(hexString: "00A79D")!
    static let gray = UIColor(hexString: "E6E6E6")!
    static let orange = UIColor(hexString: "FFBC67")!
}


struct Font {
    static let light = "SFProText-Light"
    static let regular = "SFProText-Regular"
    static let bold = "SFProText-Bold"
    static let semibold = "SFProText-Semibold"
}


struct Constant {
    static let GroupCardViewIdentifier = "GroupCardView"
    static let GameCardViewIdentifier = "GameCardView"

    
}

extension Notification.Name {
    static let play = Notification.Name("play")

}

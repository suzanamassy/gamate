//
//  RouteMAnager.swift
//  Gamers
//
//  Created by Suzan Amassy on 4/20/18.
//  Copyright © 2018 Wasim Alatrash. All rights reserved.
//

import Foundation
import UIKit


class RouteManager {
    static let storyboard = UIStoryboard(name: "Main", bundle: nil)

    static func showTopGamesVC (_ viewController : UIViewController) {
        let vc = storyboard.instantiateViewController(withIdentifier: "TopGamesVC") as! TopGamesVC
        viewController.navigationController?.pushViewController(vc, animated: true)
    }
    
    static func showAddGroupVC (_ viewController : UIViewController) {
        let vc = storyboard.instantiateViewController(withIdentifier: "AddGroupNavigationController")
        viewController.present(vc, animated: true, completion: nil)
    }
    
    static func showAGroupDetailVC (_ viewController : UIViewController, group: Group) {
        let vc = storyboard.instantiateViewController(withIdentifier: "GroupDetailVC") as! GroupDetailVC
        vc.group = group
        viewController.navigationController?.pushViewController(vc, animated: true)
    }
}

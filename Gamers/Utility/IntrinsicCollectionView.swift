//
//  IntrinsicCollectionView.swift
//  MicGamer
//
//  Created by Suzan Amassy on 4/20/18.
//  Copyright © 2018 wasimAlatrash. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class IntrinsicCollectionView: UICollectionView {
    
    override var intrinsicContentSize : CGSize {
        // Drawing code
        layoutIfNeeded()
        
        #if TARGET_INTERFACE_BUILDER
        return CGSizeMake(UIViewNoIntrinsicMetric, 300);
        #else
        return CGSize(width: UIViewNoIntrinsicMetric, height: self.contentSize.height);
        #endif
        
    }
    
    override var contentSize:CGSize {
        didSet {
            invalidateIntrinsicContentSize()
        }
    }
    
}

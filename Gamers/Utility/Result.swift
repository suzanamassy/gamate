//
//  Result.swift
//  Gamers
//
//  Created by Wasim Alatrash on 4/20/18.
//  Copyright © 2018 Wasim Alatrash. All rights reserved.
//

import Foundation

enum Result<T> {

    case success(T)
    case failure(Error)

}

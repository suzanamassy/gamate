//
//  DefaultDatabaseService.swift
//  Gamers
//
//  Created by Wasim Alatrash on 4/20/18.
//  Copyright © 2018 Wasim Alatrash. All rights reserved.
//

import Foundation
import FirebaseFirestore

final class DefaultDatabaseService: DatabaseService {

    fileprivate let defaultStore = Firestore.firestore()

}

extension DefaultDatabaseService {

    func writeObject<T: Model>(_ object: T, completion: @escaping ErrorClosure) {
        
        var document: DocumentReference!
        if object.id == nil {
            document = self.autoDocument(for: object)
        } else {
            document = self.document(for: object)
        }
        
        if var data = object.dictionary {
            data.removeValue(forKey: "id")
            document.setData(data, completion: completion)
        }
    }
    
    func updateObject<T: Model>(_ object: T, completion: @escaping ErrorClosure) {
        
        let document = self.document(for: object)
        if var data = object.dictionary {
            data.removeValue(forKey: "id")
            document.updateData(data, completion: completion)
        }
    }

    func readObject<RequestModel: Model,ResultModel: Model>(_ object: RequestModel, as: ResultModel.Type, completion: @escaping ReadClosure<ResultModel>){
        let document = self.document(for: object)
        document.getDocument { (snapshot, error) in
            if let snapshot = snapshot {
                var data = snapshot.data()
                data["id"] = object.id!
                if let object = ResultModel(dictionary: data) {
                    completion(.success(object))
                } else if let error = error  {
                    completion(.failure(error))
                }
            } else if let error = error {
                completion(.failure(error))
            } else {
                fatalError("Firestore completed a document read with neither a snapshot or error")
            }
        }
    }

    func deleteObject<T: Model>(_ object: T, completion: @escaping ErrorClosure) {
        
        let document = self.document(for: object)
        document.delete(completion: completion)
    }

}

extension DefaultDatabaseService {

    func writeObjects<T: Model>(_ objects: [T], completion: @escaping ErrorClosure) {
        let group = DispatchGroup()
        var groupError: Error?
        for object in objects {
            group.enter()
            writeObject(object) { error in
                groupError = error ?? groupError
                group.leave()
            }
        }
        group.notify(queue: .main) {
            completion(groupError)
        }
    }
    
    func updateObjects<T: Model>(_ objects: [T], completion: @escaping ErrorClosure) {
        let group = DispatchGroup()
        var groupError: Error?
        for object in objects {
            group.enter()
            updateObject(object) { error in
                groupError = error ?? groupError
                group.leave()
            }
        }
        group.notify(queue: .main) {
            completion(groupError)
        }
    }

    func readObjects<RequestModel: Model,ResultModel: Model>(_ object: RequestModel, as: ResultModel.Type, orderField: String, startingPoint: Any?, pageSize: Int, completion: @escaping ArrayReadClosure<ResultModel>){
        
        // build the query
        var query = self.collection(for: type(of: object)).order(by: orderField)
        if let startingPoint = startingPoint {
            query = query.start(after: [startingPoint])
        }
        query = query.limit(to: pageSize)
        
        // get documents
        query.getDocuments { (snapshot, error) in
            if let snapshot = snapshot {
                let documents = snapshot.documents
                var newObjectArray: [ResultModel] = []
                
                for doc in documents {
                    var data = doc.data()
                    data["id"] = doc.documentID
                    if let object = ResultModel(dictionary: data) {
                        newObjectArray.append(object)
                    }
                }
                
                completion(.success(newObjectArray))
                
            } else if let error = error {
                completion(.failure(error))
            } else {
                fatalError("Firestore completed a document read with neither a snapshot or error")
            }
        }
    }

    func deleteObjects<T: Model>(_ objects: [T], completion: @escaping ErrorClosure) {
        let group = DispatchGroup()
        var groupError: Error?
        for object in objects {
            group.enter()
            deleteObject(object) { error in
                groupError = error ?? groupError
                group.leave()
            }
        }
        group.notify(queue: .main) {
            completion(groupError)
        }
    }

}

private extension DefaultDatabaseService {

    func collection<T: Model>(for objectType: T.Type) -> CollectionReference {
        return defaultStore.collection(T.collection)
    }

    func document<T: Model>(for object: T) -> DocumentReference {
        return collection(for: type(of: object)).document(object.id!)
    }
    
    func autoDocument<T: Model>(for object: T) -> DocumentReference {
        return collection(for: type(of: object)).document()
    }
}

//
//  DatabaseService.swift
//  Gamers
//
//  Created by Wasim Alatrash on 4/20/18.
//  Copyright � 2018 Wasim Alatrash. All rights reserved.
//

import Foundation

protocol DatabaseService {

    typealias ReadClosure<T: Model> = (Result<T>) -> Void
    typealias ArrayReadClosure<T: Model> = (Result<[T]>) -> Void

    func writeObject<T: Model>(_ object: T, completion: @escaping ErrorClosure)
    func updateObject<T: Model>(_ object: T, completion: @escaping ErrorClosure)
    func readObject<RequestModel: Model,ResultModel: Model>(_ object: RequestModel, as: ResultModel.Type, completion: @escaping ReadClosure<ResultModel>)
    func deleteObject<T: Model>(_ object: T, completion: @escaping ErrorClosure)

    func writeObjects<T: Model>(_ objects: [T], completion: @escaping ErrorClosure)
    func updateObjects<T: Model>(_ objects: [T], completion: @escaping ErrorClosure)
    func readObjects<RequestModel: Model,ResultModel: Model>(_ object: RequestModel, as: ResultModel.Type, orderField: String, startingPoint: Any?, pageSize: Int, completion: @escaping ArrayReadClosure<ResultModel>)
    func deleteObjects<T: Model>(_ objects: [T], completion: @escaping ErrorClosure)

}

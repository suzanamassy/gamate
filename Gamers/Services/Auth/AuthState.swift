//
//  AuthState.swift
//  Gamers
//
//  Created by Wasim Alatrash on 4/20/18.
//  Copyright © 2018 Wasim Alatrash. All rights reserved.
//

enum AuthState {

    case signedIn(User)
    case signedOut

}

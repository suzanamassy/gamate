//
//  DefaultAuthenticationService.swift
//  Gamers
//
//  Created by Wasim Alatrash on 4/20/18.
//  Copyright © 2018 Wasim Alatrash. All rights reserved.
//

import FirebaseAuth
import Foundation

final class DefaultAuthService: AuthService {

    fileprivate let databaseService: DatabaseService
    fileprivate let defaultAuth = Auth.auth()
    fileprivate var state: AuthState = .signedOut {
        didSet {
            switch state {
            case .signedIn(let user):
                Settings.shared.userId = user.id
                Settings.shared.username = user.username
                Settings.shared.userImage = user.image
                print(user)
            default:
                Settings.shared.userId = nil
                Settings.shared.username = nil
                Settings.shared.userImage = nil
                print("signed out")
            }
        }
    }
    weak var delegate: AuthServiceDelegate?

    init(databaseService: DatabaseService) {
        self.databaseService = databaseService
    }



    func registerWith(user: User, password: String, completion: @escaping ErrorClosure) {
        defaultAuth.createUser(withEmail: user.email, password: password) { (authUser, error) in
            if let error = error {
                completion(error)
                return
            } else if let authUser = authUser {
                var newUser = user
                newUser.id = authUser.uid
                
                self.databaseService.writeObject(newUser) { error in
                    if let error = error {
                        completion(error)
                        return
                    }
                    self.state = .signedIn(newUser)
                    completion(nil)
                }
            } else {
                fatalError("Firebase Auth registration completed with neither a user or an error")
            }
        }
    }

    func signInWith(email: String, password: String, completion: @escaping ErrorClosure) {
        defaultAuth.signIn(withEmail: email, password: password) { (authUser, error) in
            if let error = error {
                completion(error)
                return
                
            } else if let authUser = authUser {
                let user = IdenitfiableUser(id: authUser.uid)
                self.databaseService.readObject(user, as: User.self ) { result in
                    switch result {
                    case .failure(let error):
                        completion(error)
                        return
                    case .success(let newUser):
                        self.state = .signedIn(newUser)
                        completion(nil)
                        return
                    }
                }
            } else {
                fatalError("Firebase Auth sign in completed with neither a user or an error")
            }
        }
    }

    func signOut(completion: @escaping ErrorClosure) {
        do {
            try defaultAuth.signOut()
            state = .signedOut
            completion(nil)
        } catch let error {
            completion(error)
        }
    }

}




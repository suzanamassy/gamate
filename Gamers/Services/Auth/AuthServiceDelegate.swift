//
//  AuthenticationServiceDelegate.swift
//  Gamers
//
//  Created by Wasim Alatrash on 4/20/18.
//  Copyright © 2018 Wasim Alatrash. All rights reserved.
//

import Foundation

protocol AuthServiceDelegate: class {

    func authService(_ service: AuthService, didUpdateStateTo state: AuthState)

}

//
//  AuthService.swift
//  Gamers
//
//  Created by Wasim Alatrash on 4/20/18.
//  Copyright © 2018 Wasim Alatrash. All rights reserved.
//

import Foundation

protocol AuthService {

    weak var delegate: AuthServiceDelegate? { get set }

    func registerWith(user: User, password: String, completion: @escaping ErrorClosure)
    func signInWith(email: String, password: String, completion: @escaping ErrorClosure)
    func signOut(completion: @escaping ErrorClosure)

}

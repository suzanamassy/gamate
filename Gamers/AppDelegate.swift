//
//  AppDelegate.swift
//  Gamers
//
//  Created by Wasim Alatrash on 4/19/18.
//  Copyright © 2018 Wasim Alatrash. All rights reserved.
//

import Firebase
import UIKit
import IQKeyboardManagerSwift



@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var authService: AuthService!
    var databaseService: DatabaseService!
    var window: UIWindow?
    
    fileprivate func LoginVC() -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateInitialViewController()!
        return controller
    }
    
    fileprivate func RootVC() -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
        return controller
    }

    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        configureServices()
        
        // enable keyboard 'Done' to hide button
        IQKeyboardManager.shared.enable = true
        
        // MARK: - Check if user login or not
        if Settings.shared.userId != nil {
            presentRootViewController(animated: false)
            
        } else {
            presentLoginViewController(animated: false)
            
        }
        
        return true
    }

}


private extension AppDelegate {
    func configureServices() {
        // Using the below FirebaseOptions instead of dropping in a .plist
        // makes it much easier to configure for multiple environments.
        FirebaseApp.configure()

        databaseService = DefaultDatabaseService()
        authService = DefaultAuthService(databaseService: databaseService)
        authService.delegate = self
    }
}



extension AppDelegate: AuthServiceDelegate {
    func authService(_ service: AuthService, didUpdateStateTo state: AuthState) {
        print("Auth service state changed to \(state)")
    }
}





/////////////////////////////////////
/////////   FOR INTERFACE   /////////
/////////////////////////////////////
extension AppDelegate {
    
    func logout() {
        authService.signOut {(error) in
            if let error = error {
                    print(error)
            
            } else {
                // Mark: - strore data in user default
                Settings.shared.userId = nil
                Settings.shared.username = nil
                Settings.shared.userImage = nil

                let appDelegate  = UIApplication.shared.delegate as! AppDelegate
                appDelegate.presentLoginViewController(animated: true)
            }
        }
    }
    
    func presentLoginViewController(animated:Bool) {
        let controller = LoginVC()

        if animated {
            UIView.transition(with: self.window!, duration: 0.1, options: [], animations: {
                self.window?.rootViewController = controller
            }, completion: nil)

        } else {
            self.window?.rootViewController = controller
        }
    }
    
    func presentRootViewController(animated:Bool) {
        let controller = RootVC()
        
        if animated {
            UIView.transition(with: self.window!, duration: 0.1, options: [], animations: {
                self.window?.rootViewController = controller
            }, completion: nil)
            
        } else {
            self.window?.rootViewController = controller
        }
    }
}

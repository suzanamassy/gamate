//
//  AddGroupVC.swift
//  Gamers
//
//  Created by Suzan Amassy on 4/21/18.
//  Copyright © 2018 Wasim Alatrash. All rights reserved.
//

import UIKit
import MBProgressHUD


class AddGroupVC: UIViewController {

    let delegate = UIApplication.shared.delegate as? AppDelegate
    fileprivate var hud : MBProgressHUD!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    @IBAction func doneButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    
    
    @IBAction func createGroupButtonPressed(_ sender: UIButton) {
        let group = Group(id: nil,
                          creatorId: Settings.shared.userId!,
                          creatorName: Settings.shared.username!,
                          creatorImage: Settings.shared.userImage!,
                          players: nil,
                          game: "Overwatch",
                          country: "Palestine",
                          creationDate: Date(),
                          device: "XBox",
                          title: "I need a sniper",
                          hasMic: true,
                          language: ["English", "Arabic"],
                          playersNumber: 0,
                          age: nil,
                          gender: "Female",
                          gameImage: "https://media.playstation.com/is/image/SCEA/overwatch-origins-edition-screen-01-ps4-us-03mar16?$MediaCarousel_Original$")
        
        delegate?.databaseService.writeObject( group, completion: { (error) in
            if let error = error {
                print(error)
       
            } else {
                print("success")
                self.dismiss(animated: true, completion: nil)
            }
        })

    }
    
    
    
    


}

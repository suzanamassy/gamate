//
//  SearchVC.swift
//  MicGamer
//
//  Created by Suzan Amassy on 4/19/18.
//  Copyright © 2018 wasimAlatrash. All rights reserved.
//

import UIKit
import MXSegmentedPager


class SearchVC: MXSegmentedPagerController, UISearchBarDelegate {
    
    lazy var searchBar = UISearchBar(frame: CGRect.zero )
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSegmentView()
        setupSearchBar()
        NotificationCenter.default.addObserver(self, selector: #selector(hideKeyboardSearchBar), name: .UIKeyboardDidHide, object: nil)
    }

    
    func setupSegmentView() {
        segmentedPager.backgroundColor = UIColor.white
        
        // Parallax Header
        segmentedPager.parallaxHeader.height = 56
        
        // Segmented Control customization
        segmentedPager.segmentedControl.selectionIndicatorHeight    = 2
        segmentedPager.segmentedControl.selectionStyle              = .textWidthStripe
        segmentedPager.segmentedControl.selectionIndicatorLocation  = .down
        segmentedPager.segmentedControl.selectionIndicatorColor     = Color.main
        
        segmentedPager.segmentedControl.titleTextAttributes =           [kCTForegroundColorAttributeName : Color.gray,
                                                                         kCTFontAttributeName: UIFont(name: Font.semibold , size: 13)!]
        segmentedPager.segmentedControl.selectedTitleTextAttributes =   [kCTForegroundColorAttributeName : Color.main]

        segmentedPager.segmentedControl.layer.shadowColor = UIColor.black.cgColor
        segmentedPager.segmentedControl.layer.shadowOpacity = 0.5
        segmentedPager.segmentedControl.layer.shadowOffset = CGSize(width: 10, height: 10)
    }
    
    
    @objc func hideKeyboardSearchBar() {
        let searchField : UITextField = searchBar.value(forKey: "searchField") as! UITextField
        searchField.endEditing(true)
    }
    
    // ShowSearchKeyboard
    func setupSearchBar() {
        searchBar.delegate = self
        
        searchBar.returnKeyType = UIReturnKeyType.search
        
        searchBar.barStyle = .black
        searchBar.backgroundColor = .black
        
        let searchField : UITextField       = searchBar.value(forKey: "searchField") as! UITextField
        searchField.backgroundColor         = UIColor.red
        searchField.layer.borderColor       = UIColor(hexString: "E0E0E9").cgColor
        searchField.layer.borderWidth       = 0.5
        searchField.layer.cornerRadius      = 6
        searchField.font                    = UIFont(name: Font.regular, size: 14)
        searchField.tintColor               = Color.main
        searchField.textColor               = UIColor(hexString: "3B3F50")
        let placeholderSetting              = NSAttributedString(string: "Search",
                                                                 attributes: [kCTForegroundColorAttributeName as NSAttributedStringKey : UIColor(hexString: "A4A4B3")])
        searchField.attributedPlaceholder   = placeholderSetting
        searchField.becomeFirstResponder()
        
        searchBar.tintColor         = Color.main
        navigationItem.titleView    = searchBar
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, titleForSectionAt index: Int) -> String {
        return ["Game", "Group"][index]
    }
    
    
    // FOR Search
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, didSelectViewWith index: Int) {
        hideKeyboardSearchBar()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        hideKeyboardSearchBar()
    }
}

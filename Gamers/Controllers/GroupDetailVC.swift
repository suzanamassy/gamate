//
//  GroupDetailVC.swift
//  Gamers
//
//  Created by Suzan Amassy on 4/21/18.
//  Copyright © 2018 Wasim Alatrash. All rights reserved.
//

import UIKit
import MBProgressHUD


class GroupDetailVC: UIViewController {
    var group:Group!
    let delegate = UIApplication.shared.delegate as? AppDelegate
    fileprivate var hud : MBProgressHUD!

    
    @IBOutlet weak var gameImageView: UIImageView!
    @IBOutlet weak var navigationButtonsStackView: UIStackView!

    // User
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var groupsCountLabel: UILabel!
    @IBOutlet weak var followButton: UIButton!

    // Game
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var memberCountLabel: UILabel!
    @IBOutlet weak var gameNameLabel: UILabel!
    @IBOutlet weak var featureStackView: UIStackView!
    @IBOutlet weak var joinView: UIView!
    @IBOutlet weak var joinButtonHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var joinChatView: UIView!


    @IBOutlet weak var membersStackView: UIStackView!
    @IBOutlet weak var joinedMemberCountLabel: UILabel!
    @IBOutlet weak var joinedMemberTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fillData()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // Show the Navigation Bar
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        // Check if user is joined or net
        self.tabBarController?.tabBar.isHidden = group.players != nil && (group.players?.filter({$0.id == Settings.shared.userId}).count)! > 0 ? false : true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        // Hide the Navigation Bar
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        // reset tab bar setting
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    fileprivate func fillData() {
        
        // User
        if let URL = URL (string: group.creatorImage) {
            userImageView.sd_setImage(with: URL)
        }
        usernameLabel.text = group.creatorName
        groupsCountLabel.text = "3"

        
        if let URL = URL (string: group.gameImage) {
            gameImageView.sd_setImage(with: URL)
        }
        titleLabel.text = group.title
        timeLabel.text = group.creationDate.shortTimeAgoSinceNow
        memberCountLabel.text = group.players != nil ? String(group.players!.count) : "0"
        gameNameLabel.text = group.game
        
        // Check if there is players in group
        membersStackView.isHidden = group.players == nil || group.players?.count == 0
        
        if group.players != nil {
            joinedMemberCountLabel.text = String(group.players!.count)
            joinedMemberTableView.reloadData()
            joinView.isHidden = group.players != nil && (group.players?.filter({$0.id == Settings.shared.userId}).count)! > 0 ? true : false
            
            joinChatView.isHidden = !joinView.isHidden
            navigationButtonsStackView.subviews.last?.isHidden = !joinView.isHidden
            joinButtonHeightConstraint.constant = joinView.isHidden ? 0 : 40
        }

        // check if feature availabele to hide or display, using subview tags (1 = mic, 2 = country, 3 = language, 4 = gender, 5 = age)
        featureStackView.viewWithTag(GroupFeature.mic.rawValue)?.isHidden = group.hasMic != nil ? group.hasMic! : false
        featureStackView.viewWithTag(GroupFeature.country.rawValue)?.isHidden = group.country != nil ? false : true
        featureStackView.viewWithTag(GroupFeature.language.rawValue)?.isHidden = group.language != nil ? false : true
        featureStackView.viewWithTag(GroupFeature.gender.rawValue)?.isHidden = group.gender != nil ? false : true
        featureStackView.viewWithTag(GroupFeature.age.rawValue)?.isHidden = group.age != nil ? false : true        
    }
    
    
    fileprivate func reloadView () {
        fillData()
        // Check if user is joined or net
        self.tabBarController?.tabBar.isHidden = group.players != nil && (group.players?.filter({$0.id == Settings.shared.userId}).count)! > 0 ? false : true

    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func inviteButtonPressed(_ sender: UIButton) {
    }

    @IBAction func shareButtonPressed(_ sender: UIButton) {
    }

    @IBAction func leaveButtonPressed(_ sender: UIButton) {
        leaveGroup()
    }
    
    @IBAction func joinChatButtonPressed(_ sender: UIButton) {
    }
    
    @IBAction func facebookButtonPressed(_ sender: UIButton) {
    }
    
    @IBAction func twitterButtonPressed(_ sender: UIButton) {
    }
    
    @IBAction func joinButtonPressed(_ sender: UIButton) {
        let alertController = UIAlertController(title: "Your level in \(group.game)", message: "", preferredStyle: .alert)

        alertController.addTextField(configurationHandler: nil)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default) { (aciton) in
            let text = alertController.textFields!.first!.text!
            print(text)
            self.joinGroup(text)
        })
        self.present(alertController, animated: true, completion: nil)
    }



    fileprivate func joinGroup(_ level : String) {
        hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.mode = .indeterminate;
        hud.removeFromSuperViewOnHide = true
        
        let player = Player(id: Settings.shared.userId!, image: Settings.shared.userImage!, name: Settings.shared.username!, level : level)
        
        if group.players == nil {
            group.players = []
        }
        
        group.players?.append(player)
        delegate?.databaseService.updateObject(group, completion: {[weak hud, weak self]  (error) in
            hud?.hide(animated: true)
            
            if error == nil {
                self?.reloadView()
            }
        })
    }
    
    
    
    fileprivate func leaveGroup() {
        group.players = group.players?.filter({ $0.id != Settings.shared.userId })
        delegate?.databaseService.updateObject(group, completion: {[weak hud, weak self]  (error) in
            hud?.hide(animated: true)
            
            if error == nil {
                self?.reloadView()
            }
        })

    }
}






extension GroupDetailVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return group.players!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CELL") as! UserTableViewCell
        cell.setData(group.players![indexPath.row])
        cell.backgroundColor = UIColor.clear
        cell.selectionStyle = .none
        return cell
    }
}

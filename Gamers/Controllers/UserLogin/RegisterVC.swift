//
//  RegisterVC.swift
//  MicGamer
//
//  Created by Suzan Amassy on 4/19/18.
//  Copyright © 2018 wasimAlatrash. All rights reserved.
//

import UIKit

class RegisterVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    // MARK: - When others RegisterVC's childViewControllers want to back to there ParentViewController (RegisterVC)
    @IBAction func unwindToRegisterVC(segue:UIStoryboardSegue) {}
}

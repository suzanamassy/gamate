//
//  LoginVC.swift
//  MicGamer
//
//  Created by Suzan Amassy on 4/19/18.
//  Copyright © 2018 wasimAlatrash. All rights reserved.
//

import UIKit
import MBProgressHUD


class LoginVC: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    fileprivate var hud : MBProgressHUD!
    @IBOutlet weak var showPasswordButton: UIButton!
    let delegate = UIApplication.shared.delegate as? AppDelegate

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        #if DEBUG
        emailTextField.text = "test@hotmail.com"
        passwordTextField.text = "Wasimjoker!?"
        #endif

        showPasswordButton.addTarget(self, action: #selector(buttonDown), for: .touchDown)
        showPasswordButton.addTarget(self, action: #selector(buttonUp), for: [.touchUpInside, .touchUpOutside])
    }
    
    
    @objc fileprivate func buttonDown(sender: AnyObject) {
        passwordTextField.isSecureTextEntry = false
    }
    
    @objc fileprivate func buttonUp(sender: AnyObject) {
        passwordTextField.isSecureTextEntry = true
    }

    
    @IBAction func loginButtonPressed(_ sender: UIButton) {
        guard self.validate() else {
            return
        }
        
        self.view.endEditing(false)

        hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.mode = .indeterminate;
        hud.removeFromSuperViewOnHide = true        
        
        delegate?.authService.signInWith(email: emailTextField.text!, password: passwordTextField.text!, completion: {[weak hud] (error) in
            if let error = error {
                hud?.mode = .text
                hud?.label.text = "Error!"
                hud?.detailsLabel.text = error.localizedDescription
                hud?.hide(animated:true, afterDelay:2.0)
          
            } else {
                // Mark: - strore data in user default
                Settings.shared.email = self.emailTextField.text
                Settings.shared.password = self.passwordTextField.text

                let appDelegate  = UIApplication.shared.delegate as! AppDelegate
                appDelegate.presentRootViewController(animated: true)
            }
        })
    }
    
    
    func validate() -> Bool {
        if (emailTextField.text?.isEmpty)! {
            hud = MBProgressHUD.showAdded(to: self.view, animated: true)
            hud?.mode = .text
            hud?.label.text = "Error!"
            hud?.detailsLabel.text = "Enter your mobile please"
            hud?.hide(animated:true, afterDelay:2.0)
            
            emailTextField.becomeFirstResponder()
            return false
            
        } else if (passwordTextField.text?.isEmpty)! {
            hud = MBProgressHUD.showAdded(to: self.view, animated: true)
            hud?.mode = .text
            hud?.label.text = "Error!"
            hud?.detailsLabel.text = "Enter password please"
            hud?.hide(animated:true, afterDelay:2.0)
            
            passwordTextField.becomeFirstResponder()
            return false
            
        } else {
            return true
        }
    }

    
    
    // MARK: - When others LoginVC's childViewControllers want to back to there ParentViewController (LoginVC)
    @IBAction func unwindToLoginVC(segue:UIStoryboardSegue) {}
}

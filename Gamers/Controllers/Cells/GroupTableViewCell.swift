//
//  GroupTableViewCell.swift
//  Gamers
//
//  Created by Suzan Amassy on 4/19/18.
//  Copyright © 2018 Wasim Alatrash. All rights reserved.
//

import UIKit
import DateToolsSwift


@objc protocol GroupTableViewCellDelegate {
    @objc func groupTableViewCellJoinButtonPressed (_ cell: GroupTableViewCell)
    @objc func groupTableViewCellLeaveButtonPressed (_ cell: GroupTableViewCell)
}


class GroupTableViewCell: UITableViewCell {
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var membersNumberLabel: UILabel!
    @IBOutlet weak var gameLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!

    @IBOutlet weak var featuresStackView: UIStackView!
    @IBOutlet weak var buttonsStackView: UIStackView!
    var delegate:GroupTableViewCellDelegate?

    @IBAction func joinButtonPressed(_ sender: UIButton) {
        delegate?.groupTableViewCellJoinButtonPressed(self)
    }
    
    @IBAction func leaveButtonPressed(_ sender: UIButton) {
        delegate?.groupTableViewCellLeaveButtonPressed(self)
    }
    
    func setData(_ group : Group) {
        questionLabel.text = group.title
        timeLabel.text = group.creationDate.shortTimeAgoSinceNow
        membersNumberLabel.text = group.players != nil ? String(group.players!.count) : "0"
        gameLabel.text = group.game
        usernameLabel.text = group.creatorName

        if let URL = URL (string: group.creatorImage) {
            userImageView.sd_setImage(with: URL)
        }
        
        // Join Button
        buttonsStackView.subviews[0].isHidden = group.players != nil && (group.players?.filter({$0.id == Settings.shared.userId}).count)! > 0 ? true : false
        
        // Joined Button
        buttonsStackView.subviews[1].isHidden = !buttonsStackView.subviews[0].isHidden
        
        // check if feature availabele to hide or display, using subview tags (1 = mic, 2 = country, 3 = language, 4 = gender, 5 = age)
        featuresStackView.viewWithTag(GroupFeature.mic.rawValue)?.isHidden = group.hasMic != nil ? group.hasMic! : false
        featuresStackView.viewWithTag(GroupFeature.country.rawValue)?.isHidden = group.country != nil ? false : true
        featuresStackView.viewWithTag(GroupFeature.language.rawValue)?.isHidden = group.language != nil ? false : true
        featuresStackView.viewWithTag(GroupFeature.gender.rawValue)?.isHidden = group.gender != nil ? false : true
        featuresStackView.viewWithTag(GroupFeature.age.rawValue)?.isHidden = group.age != nil ? false : true
    }
}

//
//  GameCollectionViewCell.swift
//  Gamers
//
//  Created by Suzan Amassy on 4/21/18.
//  Copyright © 2018 Wasim Alatrash. All rights reserved.
//

import UIKit
import SDWebImage


class GameCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var groupsNumberLabel: UILabel!
    @IBOutlet weak var thumbnailImageView: UIImageView!
    
    
    func setData(_ game : Game) {
        nameLabel.text = game.name
        
        if groupsNumberLabel != nil {
            groupsNumberLabel.text = String(game.groups)
        }
        
        
        if let URL = URL (string: game.image) {
            thumbnailImageView.sd_setImage(with: URL)
        }
    }
    
}

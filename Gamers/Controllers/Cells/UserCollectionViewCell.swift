//
//  UserCollectionViewCell.swift
//  Gamers
//
//  Created by Suzan Amassy on 4/21/18.
//  Copyright © 2018 Wasim Alatrash. All rights reserved.
//

import UIKit

class UserCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var rateNumberLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var groupsNumberLabel: UILabel!
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var groupImageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        groupImageView.tintColor = Color.orange
    }
    
    func setData(_ user : User, rate: Int) {
        rateNumberLabel.text = String(rate)
        usernameLabel.text = user.username
        groupsNumberLabel.text = String(user.numberOfGroups)
        if let URL = URL (string: user.image) {
            thumbnailImageView.sd_setImage(with: URL)
        }
    }
}


//
//  UserTableViewCell.swift
//  Gamers
//
//  Created by Suzan Amassy on 4/21/18.
//  Copyright © 2018 Wasim Alatrash. All rights reserved.
//

import UIKit

class UserTableViewCell: UITableViewCell {
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var thumbnailImageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setData(_ player : Player) {
        usernameLabel.text = player.name
        levelLabel.text = "Level : " + player.level

        if let URL = URL (string: player.image) {
            thumbnailImageView.sd_setImage(with: URL)
        }
    }

}

//
//  TabBarVC.swift
//  Gamers
//
//  Created by Suzan Amassy on 4/21/18.
//  Copyright © 2018 Wasim Alatrash. All rights reserved.
//



import UIKit
import ChameleonFramework



class TabBarVC: UITabBarController {
    // MARK: - Variable
    @IBInspectable var movingLayerColor: UIColor?
    fileprivate let movingLayerHeight:CGFloat     = 3
    fileprivate let movingLayerWidth:CGFloat     = 40
    lazy var movingLayer:CALayer = {
        let frame = CGRect(origin: .zero, size: self.movingLayerSize())
        let layer = CALayer()
        
        layer.frame = frame
        layer.backgroundColor = self.movingLayerColor?.cgColor
        layer.cornerRadius = self.movingLayerHeight/2.0
        
        layer.masksToBounds =  false
        layer.shadowColor = self.movingLayerColor?.cgColor
        
        //Here you control x and y
        layer.shadowOffset = .zero
        
        layer.shadowOpacity = 7.0
        
        //Here your control your blur
        layer.shadowRadius = 15.0
        
        let shadowPath = UIBezierPath(rect: CGRect(x: 0, y: 0, width: layer.frame.width, height: 2.5 * layer.frame.height))
        //Change 2.1 to amount of spread you need and for height replace the code for height
        layer.shadowPath = shadowPath.cgPath
        return layer
    }()
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // MARK: - to hide default preferance of tab bar (background and shadow line)
        UITabBar.appearance().shadowImage = #imageLiteral(resourceName: "MG-Tab-Bar-Shadow")
        UITabBar.appearance().backgroundImage = UIImage()
        
        // MARK: - delete text botton of tab bar item
        removeTabBarItemsText()
        setubMovingLayer()
        
        
        // MARK: - Center 'Add Group Button'
        let button: UIButton = UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: -12, width: 55, height: 55)
        button.center = CGPoint(x: tabBar.center.x , y: button.center.y)
        button.addTarget(self, action: #selector(centerButtonPressed), for: .touchUpInside)
        
        button.setBackgroundImage(#imageLiteral(resourceName: "MG-Tab-Bar-Add"), for: .normal)
        button.setBackgroundImage(#imageLiteral(resourceName: "MG-Tab-Bar-Add"), for: .highlighted)
        tabBar.addSubview(button)
    }
    
    @objc func centerButtonPressed(sender: UIButton!) {
        RouteManager.showAddGroupVC(self)
    }
    
    
    // MARK: - to remove text bottom if tab bar item
    fileprivate func removeTabBarItemsText() {
        for item in tabBar.items! {
            
            // check if item is not (+) button
            if tabBar.items?.index(of: item) != 2 {
                // remove title bottom of item
                item.title = ""
                item.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
                
                // change color of icon when selected & unselected
                let unselectedItem = [kCTForegroundColorAttributeName: Color.gray]
                let selectedItem = [kCTForegroundColorAttributeName: Color.main]
                item.setTitleTextAttributes(unselectedItem as [NSAttributedStringKey : Any], for: .normal)
                item.setTitleTextAttributes(selectedItem as [NSAttributedStringKey : Any], for: .selected)
                
            } else {
                item.imageInsets = UIEdgeInsetsMake(-10, 2, 0, 0);
                
            }
        }
    }
    
    
    // MARK: -  Need When view load on screen for first time
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if let items = tabBar.items {
            let index = selectedIndex == Int.max ? 0 : selectedIndex
            let item = items[index]
            moveMovingLayer(toItem: item, animated:false)
        }
    }
    
    
    // MARK: - to insert layer of bottom bar
    fileprivate func setubMovingLayer() {
        tabBar.layer.insertSublayer(movingLayer, at: UInt32.min)
    }
    
    
    // MARK: - return layer width and height
    fileprivate func movingLayerSize() -> CGSize {
        return CGSize(width: movingLayerWidth, height: movingLayerHeight)
    }
    
    
    // MARK: - moving bottom layer
    fileprivate func moveMovingLayer(toItem: UITabBarItem, animated:Bool) {
        movingLayer.frame.size = movingLayerSize()
        movingLayer.anchorPoint = CGPoint(x: 0.5, y: 1.0)
        movingLayer.position.y = self.tabBar.frame.size.height
        
        if animated {
            let spring = CASpringAnimation(keyPath: "position.x")
            spring.damping = 5
            spring.mass = 0.4 // Default is 1
            spring.initialVelocity = 0.9
            spring.fromValue = movingLayer.position.x
            spring.toValue = buttonCenter(item: toItem).x
            spring.duration = spring.settlingDuration
            movingLayer.position.x = self.buttonCenter(item: toItem).x
            movingLayer.add(spring, forKey: nil)
            
        } else {
            movingLayer.position.x = self.buttonCenter(item: toItem).x
            
        }
    }
    
    
    // MARK: - When tab bar item selected
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if tabBar.items?.index(of: item) != 2 {
            moveMovingLayer(toItem: item, animated: true)
        }
    }
    
    
    fileprivate func buttonCenter(item: UITabBarItem)-> CGPoint {
        let view = item.value(forKey: "view") as! UIView
        return view.center
    }
}

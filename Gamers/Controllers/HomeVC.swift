//
//  HomeVC.swift
//  Gamers
//
//  Created by Suzan Amassy on 4/21/18.
//  Copyright © 2018 Wasim Alatrash. All rights reserved.
//



import UIKit
import MBProgressHUD


class HomeVC: UIViewController {
    let delegate = UIApplication.shared.delegate as? AppDelegate
    fileprivate var hud : MBProgressHUD!
    @IBOutlet weak var gamesCollectionView: UICollectionView!
    @IBOutlet weak var gamersCollectionView: UICollectionView!
    @IBOutlet weak var groupTableView: UITableView!
    fileprivate var gamesData: [Game] = [Game]() {
        didSet {
            gamesCollectionView.reloadData()
        }
    }
    fileprivate var gamersData: [User] = [User](){
        didSet {
            gamersCollectionView.reloadData()
        }
    }
    fileprivate var groupsData: [Group] = [Group](){
        didSet {
            groupsData = groupsData.sorted(by: { $0.creationDate.compare($1.creationDate) == .orderedDescending})
            groupTableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        getData()

        // Show the Navigation Bar
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        // Hide the Navigation Bar
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
}





extension HomeVC : UITableViewDelegate, UITableViewDataSource, GroupTableViewCellDelegate {
    fileprivate func setupTableView () {
        let groupCardViewCell = UINib(nibName: Constant.GroupCardViewIdentifier, bundle: nil)
        groupTableView.register(groupCardViewCell, forCellReuseIdentifier: Constant.GroupCardViewIdentifier)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groupsData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constant.GroupCardViewIdentifier) as! GroupTableViewCell
        cell.setData(groupsData[indexPath.row])
        cell.backgroundColor = UIColor.clear
        cell.selectionStyle = .none
        
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        RouteManager.showAGroupDetailVC(self, group: groupsData[indexPath.row])
    }
    
    
    func groupTableViewCellJoinButtonPressed (_ cell: GroupTableViewCell) {
        let indexPath = groupTableView.indexPath(for: cell)!
        
        let alertController = UIAlertController(title: "Your level in \(groupsData[indexPath.row].game)", message: "", preferredStyle: .alert)
        
        alertController.addTextField(configurationHandler: nil)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default) { (aciton) in
            let text = alertController.textFields!.first!.text!
            print(text)
            self.joinGroup(indexPath, level: text)
        })
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    
    fileprivate func joinGroup(_ indexPath : IndexPath, level: String) {
        hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.mode = .indeterminate;
        hud.removeFromSuperViewOnHide = true
        
        let player = Player(id: Settings.shared.userId!, image: Settings.shared.userImage!, name: Settings.shared.username!, level : level)
        
        if groupsData[indexPath.row].players == nil {
            groupsData[indexPath.row].players = []
        }
        
        groupsData[indexPath.row].players?.append(player)
        delegate?.databaseService.updateObject(groupsData[indexPath.row], completion: {[weak hud, weak self]  (error) in
            hud?.hide(animated: true)
            
            if error == nil {
                self?.groupTableView.reloadRows(at: [indexPath], with: .automatic)
            }
        })
    }
    
    
    
    func groupTableViewCellLeaveButtonPressed (_ cell: GroupTableViewCell) {
        let indexPath = groupTableView.indexPath(for: cell)!

        groupsData[indexPath.row].players = groupsData[indexPath.row].players?.filter({ $0.id != Settings.shared.userId })
        delegate?.databaseService.updateObject(groupsData[indexPath.row], completion: {[weak hud, weak self]  (error) in
            hud?.hide(animated: true)
            
            if error == nil {
                self?.groupTableView.reloadRows(at: [indexPath], with: .automatic)
            }
        })

    }
}






extension HomeVC : UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == gamesCollectionView {
            return gamesData.count + (gamesData.count > 0 ? 1 : 0)
            
        } else if collectionView == gamersCollectionView {
            return gamersData.count + (gamersData.count > 0 ? 1 : 0)
            
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == gamesCollectionView {
            
            // check if is the last cell
            if indexPath.item < gamesData.count {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CELL", for: indexPath) as! GameCollectionViewCell
                cell.setData(gamesData[indexPath.item])
                return cell
                
            } else {
                // Create "More" Collecion view cell
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MORE-CELL", for: indexPath)
                return cell
                
                
            }
            
            
        } else  {
            // check if is the last cell
            if indexPath.item < gamersData.count {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CELL", for: indexPath) as! UserCollectionViewCell
                cell.setData(gamersData[indexPath.item], rate : indexPath.item + 1)
                return cell
                
            } else {
                // Create "More" Collecion view cell
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MORE-CELL", for: indexPath)
                return cell
                
                
            }
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == gamesCollectionView {
            RouteManager.showTopGamesVC(self)
        }
    }
    
    
    func collectionView(_ sizeForItemAtcollectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        // "top gamer" cell width size change depend on username
        if sizeForItemAtcollectionView == gamersCollectionView {
            if indexPath.item < gamersData.count {
                let user = gamersData[indexPath.item]
                let textToMeasure = user.username
                let font = UIFont(name: Font.regular , size:12)!
                let attributes = [kCTFontAttributeName: font]
                
                let rect: CGRect = textToMeasure.boundingRect( with: CGSize(width: CGFloat.greatestFiniteMagnitude, height: font.lineHeight)
                    ,options:.usesDeviceMetrics
                    ,attributes:attributes as [NSAttributedStringKey : Any]
                    ,context:nil)
                
                var size = rect.size
                size.width += 160.0
                size.height = 105.0
                
                return size
            }
            
            // "More" cell size in top gamer
            return CGSize(width: UIScreen.main.bounds.width*0.7, height: 105.0)
        }
        
        // game slider cell size
        return CGSize(width: UIScreen.main.bounds.width*0.9, height: 230)
    }
}










// MARK: - Get data
extension HomeVC {
    
    fileprivate func getData() {
        getGamesData()
        getGamersData()
        getGroupsData()
    }
    
    fileprivate func getGamesData() {
        hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.mode = .indeterminate;
        hud.removeFromSuperViewOnHide = true
        
        delegate?.databaseService.readObjects(IdenitfiableGame(), as: Game.self, orderField: "groups", startingPoint: nil, pageSize: 20, completion: {[weak hud, weak self] (result) in
            hud?.hide(animated: true)
            
            switch result {
            case .success(let data):
                self?.gamesData = data
                print("gamesData", data)
                
            case .failure(let error):
                print(error)
            }
        })
    }
    
    
    fileprivate func getGamersData() {
        hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.mode = .indeterminate;
        hud.removeFromSuperViewOnHide = true
        
        delegate?.databaseService.readObjects(IdenitfiableUser(), as: User.self, orderField: "numberOfGroups", startingPoint: nil, pageSize: 20, completion: {[weak hud, weak self] (result) in
            hud?.hide(animated: true)
            
            switch result {
            case .success(let data):
                self?.gamersData = data
                print("gamersData", data)
                
            case .failure(let error):
                print(error)
            }
        })
    }
    
    fileprivate func getGroupsData() {
        hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.mode = .indeterminate;
        hud.removeFromSuperViewOnHide = true
        
        delegate?.databaseService.readObjects(IdenitfiableGroup(), as: Group.self, orderField: "creationDate", startingPoint: nil, pageSize: 20, completion: {[weak hud, weak self] (result) in
            hud?.hide(animated: true)
            
            switch result {
            case .success(let data):
                self?.groupsData = data
                print("groupsData", data)
                
            case .failure(let error):
                print(error)
            }
        })
    }
    
    
    
}

//
//  TopGamesVC.swift
//  Gamers
//
//  Created by Suzan Amassy on 4/21/18.
//  Copyright © 2018 Wasim Alatrash. All rights reserved.
//



import UIKit
import MBProgressHUD
import CHTCollectionViewWaterfallLayout


class TopGamesVC: UIViewController {
    
    let delegate = UIApplication.shared.delegate as? AppDelegate
    fileprivate var hud : MBProgressHUD!
    fileprivate var gamesData: [Game] = [Game]() {
        didSet {
            collectionView.reloadData()
        }
    }
    
    @IBOutlet weak var collectionView: UICollectionView!
    lazy var collectionViewLayout:CHTCollectionViewWaterfallLayout = {
        let _collectionViewLayout = CHTCollectionViewWaterfallLayout()
        _collectionViewLayout.minimumColumnSpacing    = 5
        _collectionViewLayout.minimumInteritemSpacing = 5
        
        return _collectionViewLayout
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getData()
        setupCollectionView()
    }
}




extension TopGamesVC : UICollectionViewDataSource, CHTCollectionViewDelegateWaterfallLayout {
    
    func setupCollectionView() {
        /// setup collection view layout 2 element per row ....
        collectionView.collectionViewLayout = collectionViewLayout
        setLayoutColumnCount(size: collectionView.bounds.width)
        
        // add collectionView Cell (Xib) file to collectionView
        let gameCardViewCell = UINib(nibName: Constant.GameCardViewIdentifier, bundle: nil)
        collectionView.register(gameCardViewCell, forCellWithReuseIdentifier: Constant.GameCardViewIdentifier)
        
        
        collectionView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
    }
    
    func setLayoutColumnCount(size withSize:CGFloat) {
        let itemsPerLine = Int(floor(withSize / (withSize/2)))
        collectionViewLayout.columnCount = itemsPerLine
    }
    
    func collectionView (_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 194, height: 316)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return gamesData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constant.GameCardViewIdentifier, for: indexPath) as! GameCollectionViewCell
        cell.setData(gamesData[indexPath.item])
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
}












// MARK: - Get data
extension TopGamesVC {
    fileprivate func getData() {
        getGamesData()
    }
    
    fileprivate func getGamesData() {
        hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.mode = .indeterminate;
        hud.removeFromSuperViewOnHide = true
        
        delegate?.databaseService.readObjects(IdenitfiableGame(), as: Game.self, orderField: "groups", startingPoint: nil, pageSize: 20, completion: {[weak hud, weak self] (result) in
            hud?.hide(animated: true)
            
            switch result {
            case .success(let data):
                self?.gamesData = data
                print("gamesData", data)
                
            case .failure(let error):
                print(error)
            }
        })
    }
}

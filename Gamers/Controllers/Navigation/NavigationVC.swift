//
//  NavigationVC.swift
//  Gamers
//
//  Created by Suzan Amassy on 4/21/18.
//  Copyright © 2018 Wasim Alatrash. All rights reserved.
//

import UIKit
import ChameleonFramework



class NavigationVC: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Navigation Bar title setting color, font size and family
        let barTitleTextAttributes:[NSAttributedStringKey : Any] = [kCTForegroundColorAttributeName as NSAttributedStringKey:   UIColor(hexString : "3B3F50"),
                                                                    kCTFontAttributeName as NSAttributedStringKey :             UIFont(name: Font.semibold , size: 16)!]
        
        UINavigationBar.appearance(whenContainedInInstancesOf: [NavigationVC.self]).titleTextAttributes = barTitleTextAttributes
        UINavigationBar.appearance(whenContainedInInstancesOf: [NavigationVC.self]).barTintColor = .white
        UINavigationBar.appearance(whenContainedInInstancesOf: [NavigationVC.self]).tintColor = .black
        
        // Hide 1 px shadow in navigation
        UINavigationBar.appearance(whenContainedInInstancesOf: [NavigationVC.self]).shadowImage = UIImage()
        
        // Hide "Back" Button Text
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [NavigationVC.self]).setBackButtonTitlePositionAdjustment(UIOffsetMake(-100000, 0), for:.default)
        
        // Change Back Arrow icon
        UINavigationBar.appearance(whenContainedInInstancesOf: [NavigationVC.self]).backIndicatorImage = #imageLiteral(resourceName: "MG-Back-Arrow").withAlignmentRectInsets(UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0))
        UINavigationBar.appearance(whenContainedInInstancesOf: [NavigationVC.self]).backIndicatorTransitionMaskImage = #imageLiteral(resourceName: "MG-Back-Arrow").withAlignmentRectInsets(UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0))
        
        
        navigationBar.layer.shadowColor = UIColor(hexString: "1D202E").cgColor
        navigationBar.layer.shadowOffset = CGSize(width: 0.0, height: 4.0)
        navigationBar.layer.shadowRadius = 24.0
        navigationBar.layer.shadowOpacity = 0.2
        navigationBar.layer.masksToBounds = false
    }
}
